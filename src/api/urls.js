
import baseURL from 'apiurl';

export const providerURLS = {
    fetchProviders: `${baseURL}providers`,
    fetchProviderRequests: `${baseURL}providers/requests`,
    addService: `${baseURL}provider/addservice`,
    removeProviderService: `${baseURL}provider/removeservice`
}

export const clientURLS = {

}

export const serviceURLS = {
    searchServices: `${baseURL}services/search`,
    searchServicesToAdd: `${baseURL}services/search/toadd`,
    getProvidersNearBy: `${baseURL}request`,
    requestService:`${baseURL}request/requestservice`,
}

export const requestURLS = {

}

export const userURLS = {
    authenticate: `${baseURL}authenticate`,
    creteClient: `${baseURL}client`,
    createProvider: `${baseURL}provider`
}