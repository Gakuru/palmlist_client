import React from 'react';

export const Text = ({
    onChange,
    value,
    label,
    name,
    error,
    required = false
}) => {
    return (
        <div className="form-group">
            <label htmlFor={name}>
                {label}
            </label>
            <input
                className="form-control"
                name={name}
                type="text"
                value={value}
                required={required}
                onChange={onChange}/>
        </div>
    );
}
export const Password = ({
    onChange,
    value,
    label,
    name,
    error,
    required = false
}) => {
    return (
        <div className="form-group">
            <label htmlFor={name}>
                {label}
            </label>
            <input
                className="form-control"
                name={name}
                type="password"
                value={value}
                required={required}
                onChange={onChange}/>
        </div>
    );
}
export const Email = ({
    onChange,
    value,
    label,
    name,
    error,
    required = false
}) => {
    return (
        <div className="form-group">
            <label htmlFor={name}>
                {label}
            </label>
            <input
                className="form-control"
                name={name}
                type="email"
                value={value}
                required={required}
                onChange={onChange}/>
        </div>
    );
}
export const Tel = ({
    onChange,
    value,
    label,
    name,
    error,
    required = false
}) => {
    return (
        <div className="form-group">
            <label htmlFor={name}>
                {label}
            </label>
            <input
                className="form-control"
                name={name}
                type="tel"
                value={value}
                required={required}
                onChange={onChange}/>
        </div>
    );
}

export const Checkbox = ({
    onChange,
    value,
    label,
    name,
    error,
    checked = false
}) => {
    return (
        <div className="form-group">
            <label htmlFor={name}>
                {label}
            </label>
            &emsp;
            <input
                className="checkbox"
                name={name}
                type="checkbox"
                value={value}
                defaultChecked={checked}
                onChange={(e,) => {
                e.target.value = e.target.checked;
                onChange(e);
            }}/>
        </div>
    );
}