import {createStore, applyMiddleware, combineReducers} from 'redux';
import {reducer as formReducer} from 'redux-form';
import thunk from 'redux-thunk';

import {signupReducer} from '../components/user/signup/reducer';
import {loginReducer} from '../components/user/login/reducer';
import { findServiceReducer, findProvidersNearByReducer } from '../components/sevices/reducer';
import {providerServices,providerRequests} from '../components/provider/reducer';

const rootReducer = combineReducers(
    {
        signup: signupReducer,
        login: loginReducer,
        service: findServiceReducer,
        providersNearBy: findProvidersNearByReducer,
        form: formReducer,
        providerServices: providerServices,
        providerRequests:providerRequests
    });

const middleware = applyMiddleware(thunk);

export const store = createStore(rootReducer, middleware);