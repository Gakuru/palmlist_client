import React from 'react';
import { Redirect } from 'react-router';

export const user = (() => {
    const token = window.localStorage.palmlistToken;

    if (token) {
        var base64Url = token.split('.')[1];
        var base64 = base64Url.replace('-', '+').replace('_', '/');
    
        return JSON.parse(window.atob(base64)).user;
    } else {
        return null;
    }

})();

export const setupUser = (user) => {
    if (user) {
        if (user.success) {
            let palmlist = window.localStorage;
            palmlist.setItem('palmlistToken', user.token);
            return (<Redirect to='/' />);
        }
    }
}

export const logoutUser = () => {
    let palmlist = window.localStorage;
    palmlist.removeItem('palmlistToken');
    window.location = '/login';
}