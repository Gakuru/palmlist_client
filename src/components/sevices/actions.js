import * as types from './actionTypes';
import axios from 'axios';

import { serviceURLS,providerURLS } from '../../api/urls';

const findServicesSuccess = (service) => {
    return {type: types.FIND_SERVICES_SUCCESS, service};
}

const findServicesFailed = (service) => {
    return {type: types.FIND_SERVICES_FAILED, service};
}

const findServicesToAddSuccess = (service) => {
    return {type: types.FIND_SERVICES_SUCCESS, service};
}

const findServicesToAddFailed = (service) => {
    return {type: types.FIND_SERVICES_FAILED, service};
}

const findProvidersNearBySuccess = (providers) => {
    return {type: types.FIND_PROVIDERS_NEARBY_SUCCESS, providers};
}

const findProvidersNearByFailed = (providers) => {
    return {type: types.FIND_PROVIDERS_NEARBY_FAILED, providers};
}

export const findService = (params) => {
    return (dispatch) => {
        return axios
            .get(`${serviceURLS.searchServices}?service=${params.service}`)
            .then((_response) => {
                const response = _response.data;
                if (response.success) 
                    dispatch(findServicesSuccess(response));
                else 
                    dispatch(findServicesFailed(response));
                }
            )
            .catch((error) => {
                throw error;
            });
    }
}

export const findServiceToAdd = (params) => {
    return (dispatch) => {
        return axios
            .get(`${serviceURLS.searchServicesToAdd}?service=${params.service}&pid=${params.pid}`)
            .then((_response) => {
                const response = _response.data;
                if (response.success) 
                    dispatch(findServicesToAddSuccess(response));
                else 
                    dispatch(findServicesToAddFailed(response));
                }
            )
            .catch((error) => {
                throw error;
            });
    }
}

export const addServiceToProvider = (data) => {
    return (dispatch) => {
        return axios
            .post(`${providerURLS.addService}`, data)
            .then((_response) => {
                const response = _response.data;
                if (response.success)
                    console.info('service added');
                else
                    console.info('unable to add service');
                })
            .catch((error) => {
                throw error;
        });
    }
}

export const findProvidersNearBy = (params) => {
    
    return (dispatch) => {
        return axios
            .get(`${serviceURLS.getProvidersNearBy}?lat=${params.lat}&lng=${params.lng}&sid=${params.sid}`)
            .then((_response) => {
                const response = _response.data;
                if (response.success) 
                    dispatch(findProvidersNearBySuccess(response));
                else 
                    dispatch(findProvidersNearByFailed(response));
                }
            )
            .catch((error) => {
                throw error;
            });
    }
}