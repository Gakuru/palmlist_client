import React from 'react';

import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';

import GoogleMapReact from 'google-map-react';

import * as serviceActions from './actions';

import { user } from '../../utils/util';
import NavBar from '../../layout/navbar';

const AnyReactComponent = ({ text }) => (
    <div style={{
      position: 'relative', color: 'white', background: 'red',
      height: 40, width: 60, top: -20, left: -30,    
    }}>
      {text}
    </div>
);

class FindServiceNearBy extends React.Component {

    constructor(props) {
        super(props);

        this.entity = {};
        this.coordinates = {};

        this.state = {
            isSearching: false,
            locationFound:false,
            service: props.service,
            providersNearBy: props.providersNearBy
        }

        this.defaultCoords = {
            center: {
                lat: 0,
                lng: 0,
            },
            zoom: 15
          };

        this.handleChange = this
            .handleChange
            .bind(this);
        this.handleFindService = this
            .handleFindService
            .bind(this);

        this.renderSuggestions = this
            .renderSuggestions
            .bind(this);

        this.getServiceProvidersNearby = this
            .getServiceProvidersNearby
            .bind(this);

        this.toggleIsSearching = this
            .toggleIsSearching
            .bind(this);

        this.renderMapCanvas = this.renderMapCanvas.bind(this);
        
        this.mapProvidersNearBy = this
            .mapProvidersNearBy
            .bind(this);
        
        this.markProvidersNearBy = this.markProvidersNearBy.bind(this);

        this.handleLocationChanged = this.handleLocationChanged.bind(this);

        this.getCurrentLocation = this.getCurrentLocation.bind(this);

        this.listProviders = this.listProviders.bind(this);

        this.listProvider = this.listProvider.bind(this);

        this.handleRequestClick = this.handleRequestClick.bind(this);
    }

    componentDidMount() {
        this.getCurrentLocation();
    }

    getCurrentLocation() {
        this.setState({ locationFound: false });
        if (navigator.geolocation) {
            console.info('Attempting to get current location...');
            navigator.geolocation.getCurrentPosition((position) => {

                console.info('Location found...');

                this.entity.lat = position.coords.latitude;
                this.entity.lng = position.coords.longitude;

                this.defaultCoords.center.lat = position.coords.latitude;
                this.defaultCoords.center.lng = position.coords.longitude;

                this.setState({ locationFound: true });
            });
        } else {
            console.info('Geolocation is not supported by this browser.');
        }
    }

    handleChange(e) {

        this.toggleIsSearching(e.target.value.length
            ? true
            : false);       
        
        this.entity[e.target.name] = e.target.value;

        this.handleFindService();
    }

    toggleIsSearching(val) {
        this.setState({isSearching: val});
    }

    handleFindService() {

        this.setState({
            service: this
                .props
                .serviceActions
                .findService(this.entity)
        });
    }

    handleLocationChanged(coords) {
        if (coords) {
            this.entity.lat = coords.latitude;
            this.entity.lng = coords.longitude;

            this.defaultCoords.center.lat = coords.latitude;
            this.defaultCoords.center.lng = coords.longitude;            
            
        } else {
            console.info('no coordinates');
       }
    }

    handleRequestClick(pid) {
        let sid = this.entity.sid;
        
        // Todo add provider_id to entity
        let values = {
            pid: pid,
            sid: sid,
            uid: user.id,
            lat: this.entity.lat,
            lng: this.entity.lng
        }

        this.props.serviceActions.requestService(values);

    }

    getServiceProvidersNearby(event, sid, service) {
        event.preventDefault();

        let entity = {
            lat: this.entity.lat,
            lng: this.entity.lng,
            sid: sid
        }

        this.entity.sid = sid;

        this.setState({
            service: this
                .props
                .serviceActions
                .findProvidersNearBy(entity)
        });

        this.setState({isSearching: false});
        this.entity.service = service;
    }

    renderSuggestions() {
        if (this.props.service && this.state.isSearching) {
            const {services} = this.props.service.results;
            return (
                <div className="list-group list-group-flush">
                    {services.map(service => {
                        return (
                            <a
                                href=""
                                className="list-group-item"
                                key={service.id}
                                onClick={(e) => this.getServiceProvidersNearby(e, service.id, service.name)}>
                                {service.name}
                            </a>
                        )
                    })}
                </div>
            )
        }
    }

    renderMapCanvas(markers = false, points = []) {
        let recenter = {
            lat: undefined,
            lng:undefined,
        }
        if (points.length) {
            recenter.lat = parseFloat(points[0].lat);
            recenter.lng = parseFloat(points[0].lng);
        }

        if (this.state.locationFound) {
            return (
                // Important! Always set the container height explicitly
                <div style={{ height: '84vh', width: '100%' }}>
                  <GoogleMapReact
                        //Add API KEY HERE
                        center={points.length?recenter:this.defaultCoords.center}
                        zoom={points.length?12:this.defaultCoords.zoom}
                    >
    
                  {markers?this.markProvidersNearBy(points):null}
    
                  </GoogleMapReact>
                </div>
              );
        }
    }

    mapProvidersNearBy() {
        if (this.props.providersNearBy) {
            const { providers } = this.props.providersNearBy.results;
            
            return this.renderMapCanvas(true,providers);

        } else {
            return this.renderMapCanvas();
        }
    }

    markProvidersNearBy(providers) {
        return providers.map(provider => {
            return (
                <AnyReactComponent
                        key={provider.id}
                        lat={provider.lat}
                        lng={provider.lng}
                        text={provider.name}
                    />
            );
        })
    }

    listProviders() {        
        if (this.props.providersNearBy) {
            const { providers } = this.props.providersNearBy.results;
            if (providers.length) {
                return (
                    <div style={{ position: "absolute", bottom: 10, zIndex: 2, width: "60%", maxHeight: 150, overflowY: "auto" }} className="alert alert-dark">
                        {this.listProvider(providers)}
                    </div>
                )
            }
        }   
    }

    listProvider(providers) {
        return providers.map((provider) => {
            return (
                <div key={provider.id} className="row request-list">
                    <div className="col-2">
                        {3} <i className="fas fa-thumbs-up"></i>
                    </div>
                    <div className="col-4">
                        {provider.name}
                    </div>
                    <div className="col-3">
                        {provider.distance} Km away
                    </div>
                    <div className="col-3">
                        <button className="btn btn-primary btn-sm float-right" onClick={()=>this.handleRequestClick(provider.id)}>Request</button>
                    </div>
                </div>
           );
        });
    }

    render() {
        return (
            <div className="container-fluid">
                <NavBar />
                <div>
                    <div>
                        <input
                            type="text"
                            className="form-control"
                            name="service"
                            value={this.entity.service || ''}
                            onChange={this.handleChange}
                            placeholder="Find service providers near you"/>
                        <div className="suggest well well-sm" style={{zIndex:1,position:'absolute',top:'20.5%',width:'100%'}}>
                            {this.renderSuggestions()}
                        </div>
                    </div>

                    {this.mapProvidersNearBy()}
                </div>

                {this.listProviders()}

            </div>
        )
    }
}

const mapStateToProps = (store, ownProps) => {
    return {service: store.service.data, providersNearBy: store.providersNearBy.data}
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        serviceActions: bindActionCreators(serviceActions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(FindServiceNearBy);