import * as types from './actionTypes';

export const findServiceReducer = (state = {}, action) => {
    switch (action.type) {
        case types.FIND_SERVICES_SUCCESS:
            {
                return {
                    ...state,
                    data: action.service
                };
            }
        case types.FIND_SERVICES_FAILED:
            {
                return {
                    ...state,
                    data: action.service
                };
            }
        default:
            {
                return state;
            }
    }
}

export const findProvidersNearByReducer = (state = {}, action) => {
    switch (action.type) {
        case types.FIND_PROVIDERS_NEARBY_SUCCESS:
            {
                return {
                    ...state,
                    data: action.providers
                };
            }
        case types.FIND_PROVIDERS_NEARBY_FAILED:
            {
                return {
                    ...state,
                    data: action.providers
                };
            }
        default:
            {
                return state;
            }
    }
}