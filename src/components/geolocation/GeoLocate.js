import React from 'react';
import {geolocated} from 'react-geolocated';
 
class GeoLocate extends React.Component {

  componentDidUpdate() {    
    this.props.handleLocationChanged(this.props.coords);
  }

  render() {
    return !this.props.isGeolocationAvailable
      ? <div>Your browser does not support Geolocation</div>
      : !this.props.isGeolocationEnabled
        ? <div>Geolocation is not enabled</div>
        : this.props.coords
          ? <span></span>
          : <div>Getting the location data&hellip; </div>;
  }
}
 
export default geolocated({
  positionOptions: {
    enableHighAccuracy: false,
  },
  userDecisionTimeout: 5000,
})(GeoLocate);  