import * as types from './actionTypes';

export const signupReducer = (state = {}, action) => {
    switch (action.type) {
        case types.CREATE_USER_SUCCESS:
            {
                return {
                    ...state,
                    data: action.user
                };
            }
        case types.CREATE_USER_FAILED:
            {
                return {
                    ...state,
                    data: action.user
                };
            }
        default:
            {
                return state;
            }
    }
}