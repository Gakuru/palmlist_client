import React from 'react';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import {SignUpForm} from '../../../dummies/user/SignUpForm';

import * as signUpActions from './actions';

class Signup extends React.Component {

    constructor(props) {
        super(props);
        this.entity = {};

        this.state = {
            user: props.user
        }

        this.handleInputChange = this
            .handleInputChange
            .bind(this);

        this.handleSignup = this
            .handleSignup
            .bind(this);

    }

    handleInputChange(e) {
        this.entity[e.target.name] = e.target.value;
    }

    handleSignup(e) {
        e.preventDefault();
        this.setState({
            user: this
                .props
                .signUpActions
                .saveUser(this.entity)
        });
    }

    render() {
        return (
            <div>
                <SignUpForm
                    user={this.props.user}
                    handleSignup={this.handleSignup}
                    handleInputChange={this.handleInputChange}/>
            </div>
        )
    }

}
const mapStateToProps = (store, ownProps) => {
    return {user: store.signup}
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        signUpActions: bindActionCreators(signUpActions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Signup);