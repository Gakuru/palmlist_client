import * as types from './actionTypes';
import axios from 'axios';

import { userURLS } from '../../../api/urls';

const createUserSuccess = (user) => {
    return {type: types.CREATE_USER_SUCCESS, user};
}

const createUserFailed = (user) => {
    return {type: types.CREATE_USER_FAILED, user};
}

export const saveUser = (user) => {
    let isProvider = user.isProvider;
    let url = userURLS.creteClient;
    if (isProvider && Boolean(isProvider)) {
        url = userURLS.createProvider;
    }
    return (dispatch) => {
        return axios
            .post(`${url}`, user)
            .then((_response) => {
                const response = _response.data;
                if (response.success) 
                    if (isProvider && Boolean(isProvider)) 
                        dispatch(createUserSuccess(response));
                    else 
                        dispatch(createUserSuccess(response));
            else 
                    dispatch(createUserFailed(response));
                }
            )
            .catch((error) => {
                throw error;
            });
    }
}