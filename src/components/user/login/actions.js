import * as types from './actionTypes';
import axios from 'axios';

import { userURLS } from '../../../api/urls';

const loginUserSuccess = (user) => {
    return {type: types.LOGIN_USER_SUCCESS, user};
}

const loginUserFailed = (user) => {
    return {type: types.LOGIN_USER_FAILED, user};
}

export const loginUser = (user) => {
    return (dispatch) => {
        return axios
            .post(`${userURLS.authenticate}`, user)
            .then((_response) => {
                const response = _response.data;
                if (response.success)
                    dispatch(loginUserSuccess(response));
                    
                else 
                    dispatch(loginUserFailed(response));
                }
            )
            .catch((error) => {
                throw error;
            });
    }
}