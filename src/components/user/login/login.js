import React from 'react'

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import * as LoginActions from './actions';

import { LoginForm } from '../../../dummies/user/LoginForm';

import { Redirect } from 'react-router';

import { user,setupUser } from '../../../utils/util';

class Login extends React.Component {

    constructor(props) {
        super(props);

        this.entity = {};

        this.state = {
            user: props.user
        };

        this.handleInputChange = this
            .handleInputChange
            .bind(this);
        this.handleLogin = this
            .handleLogin
            .bind(this);
        
        this.renderLoginForm = this.renderLoginForm.bind(this);
    }

    handleInputChange(e) {
        this.entity[e.target.name] = e.target.value;
    }

    handleLogin(e) {
        e.preventDefault();
        this.setState({
            user: this
                .props
                .LoginActions
                .loginUser(this.entity)
        });
    }

    renderLoginForm() {
        if (user) {
            return (<Redirect to={'/'} />);
        } else {
            return (
                <div>
                    <LoginForm
                    handleLogin={this.handleLogin}
                    handleInputChange={this.handleInputChange}
                    user={this.props.user} />
                    
                    {setupUser(this.props.user)}
                </div>
            );            
        }
    }

    render() {
        return (
            <div>
                {this.renderLoginForm()}
            </div>
        )
    }

}

const mapStateToProps = (store, ownProps) => {
    return {user: store.login.data}
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        LoginActions: bindActionCreators(LoginActions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);