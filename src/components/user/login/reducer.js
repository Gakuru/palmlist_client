import * as types from './actionTypes';

export const loginReducer = (state = {}, action) => {
    switch (action.type) {
        case types.LOGIN_USER_SUCCESS:
            {
                return {
                    ...state,
                    data: action.user
                };
            }
        case types.LOGIN_USER_FAILED:
            {
                return {
                    ...state,
                    data: action.user
                };
            }
        default:
            {
                return state;
            }
    }
}