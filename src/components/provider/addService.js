import React from 'react';

import { user } from '../../utils/util';
import NavBar from '../../layout/navbar';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import { Link } from 'react-router-dom';

import * as ServiceActions from '../sevices/actions';

class AddService extends React.Component{

    constructor(props) {
        super(props);

        this.isSearching = false;

        this.state = {
            service: props.service
        };

        this.handleEventChanged = this.handleEventChanged.bind(this);
        
        this.renderServices = this.renderServices.bind(this);

        this.addService = this.addService.bind(this);
    }

    handleEventChanged(event) {
        event.preventDefault();
        let value = event.target.value;
        this.isSearching = value.length ? true : false;
        this.setState({service: this.props.ServiceActions.findServiceToAdd({service:value,pid:user.id})});
    }

    addService(sid) {
        // this.setState({ service: this.props.ServiceActions.addServiceToProvider({ sid: sid, pid: user.id }) });
        
        // console.info(this.props.service);

        let services = this.props.service.results.services;

        console.info(services);

        services.filter((service, i) => {            
            if (service.id === sid) {
                console.info(service.id, i);
                services.splice(i, 0);

                console.info(services);
            }                   
        });
    }

    renderServices() {
        if (this.props.service && this.isSearching) {
            const { services } = this.props.service.results;
            return services.map((service) => {
                return (
                    <div key={service.id} id={service.id}>
                        <div className="card bg-light p-3">
                            <div className="row">
                                <div className="col-6">
                                    {service.name}
                                </div>
                                <div className="col-6 text-right">
                                    <button className="btn btn-primary btn-sm" onClick={()=>this.addService(service.id)}>
                                        Add
                                    </button>
                                </div>
                            </div>
                        </div>
                        <br />
                    </div>
                );
            })
        }
    }

    render() {
        return (
            <div className="container-fluid">
            <div className="row">
                <div className="col-12">
                    <NavBar />
                </div>
                    <div className="col-12">
                    <hr />
                    <div className="row breadcrumb">
                        <div className="col-6">
                            <h5>
                                Add New Services To Your Collection
                            </h5>
                        </div>
                        <div className="col-6">
                            <Link to={`/${(user?user.lastName.toLowerCase():'')}/services`} className="btn btn-danger btn-sm float-right">Cancel</Link>
                        </div>
                    </div>
                    <hr />
                </div>
                <div className="col-12">
                    <div className="form-group">
                        <input type="text" placeholder="Search for services to add..." className="form-control" onChange={this.handleEventChanged} />
                    </div>
                </div>
                <div className="col-12">
                    {this.renderServices()}
                </div>    
            </div>
        </div>
        );
    }
}

const mapStateToProps = (store, ownProps) => {
    return {service: store.service.data}
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        ServiceActions: bindActionCreators(ServiceActions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddService);
