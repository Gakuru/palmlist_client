import * as types from './actionTypes';
import axios from 'axios';

import { providerURLS } from '../../api/urls';

const fetchProviderServicesSuccess = (services) => {
    return {type: types.FETCH_PROVIDER_SERVICES_SUCCESS, services};
}

const fetchProviderServicesFailed = (services) => {
    return {type: types.FETCH_PROVIDER_SERVICES_FAILED, services};
}

const fetchProviderRequestsSuccess = (requests) => {
    return {type: types.FETCH_PROVIDER_REQUESTS_SUCCESS, requests};
}

const fetchProviderRequestsFailed = (requests) => {
    return {type: types.FETCH_PROVIDER_REQUESTS_FAILED, requests};
}

export const getProviderServices = (id) => {
    return (dispatch) => {
        return axios
            .get(`${providerURLS.fetchProviders}/${id}/services`)
            .then((_response) => {
                const response = _response.data;
                if (response.success)
                    dispatch(fetchProviderServicesSuccess(response.results.services));
                else 
                    dispatch(fetchProviderServicesFailed(response));
                }
            )
            .catch((error) => {
                throw error;
            });
    }
}

export const removeProviderService = (data) => {
    return (dispatch) => {
        return axios
            .post(providerURLS.removeProviderService,data)
            .then((_response) => {
                const response = _response.data;
                if (response.success)
                    console.info('service removed');
                else 
                    console.info('service could not removed');
                }
            )
            .catch((error) => {
                throw error;
            });
    }
}

export const getProviderRequests = (id) => {
    return (dispatch) => {
        return axios
            .get(`${providerURLS.fetchProviderRequests}/${id}`)
            .then((_response) => {
                const response = _response.data;
                if (response.success) {
                    dispatch(fetchProviderRequestsSuccess(response.results.requests));
                } else {
                    dispatch(fetchProviderRequestsFailed(response));
                }
        });
    }
}