import React from 'react';

import { user } from '../../utils/util';
import NavBar from '../../layout/navbar';

import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router-dom';

import * as providerActions from './actions';

class ProviderServices extends React.Component{

    constructor(props) {
        super(props);

        this.state = {
            providerServices: {}
        };

        this.getProviderServices = this.getProviderServices.bind(this);
        this.renderProviderServices = this.renderProviderServices.bind(this);
        this.removeProviderService = this.removeProviderService.bind(this);
    }

    componentDidMount() {
        this.getProviderServices();
    }

    getProviderServices() {
        this.setState({providerServices: this.props.providerActions.getProviderServices(user.id)});
    }

    removeProviderService(id) {
        this.props.providerActions.removeProviderService({id:id,pid:user.id});
    }

    renderProviderServices() {
        if (this.props.providerServices) {
            const { providerServices } = this.props;

            return providerServices.map(service => {
                return (
                    <div key={service.id} className="row card" style={{ marginTop: 5, marginLeft: 1,marginRight:1}}>
                        
                        <div className="col-12">
                            <div className="row">
                                <div className="col-1">
                                    Service:
                                </div>
                                <div className="col">
                                    {service.name}
                                </div>
                            </div>
                        </div>
                        
                        <div className="col-12">
                            <hr/>
                        </div>

                        <div className="col-12">
                            <div className="row">
                                <div className="col-1">
                                    Tags:
                                </div>
                                <div className="col">
                                    {service.tags}
                                </div>
                            </div>
                        </div>
                        
                        <div className="col-12">
                            <hr/>
                        </div>

                        <div className="col-12">
                            <div className="row float-right">
                                <div className="col-1">
                                    <button className="btn btn-danger btn-sm" onClick={()=>this.removeProviderService(service.id)}>Remove</button>
                                </div>
                            </div>
                        </div>

                        <div className="col-12">
                            <br/>
                        </div>

                    </div>
                );
            });

        } else {
            return null;
        }
    }

    render() {

        return (
            <div className="container-fluid">
            <div className="row">
                <div className="col-12">
                    <NavBar />
                </div>
                    <div className="col-12">
                    <hr />        
                    <div className="row breadcrumb">
                        <div className="col">
                            Services You Offer
                        </div>
                        <div>
                            <Link to={`/${(user?user.lastName.toLowerCase():'')}/services/add`} className="btn btn-primary float-right">Add a Service</Link>
                        </div>
                    </div>
                </div> 
                <div className="col-12">
                    {this.renderProviderServices()}
                </div>
            </div>
        </div>
        );
    }
}

const mapStateToProps = (store, ownProps) => {
    return {providerServices: store.providerServices.data}
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        providerActions: bindActionCreators(providerActions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProviderServices);
