import * as types from './actionTypes';

export const providerServices = (state = {}, action) => {
    switch (action.type) {
        case types.FETCH_PROVIDER_SERVICES_SUCCESS:
            {
                return {
                    ...state,
                    data: action.services
                };
            }
        case types.FETCH_PROVIDER_SERVICES_FAILED:
            {
                return {
                    ...state,
                    data: action.services
                };
            }
        default:
            {
                return state;
            }
    }
}

export const providerRequests = (state = {}, action) => {
    switch (action.type) {
        case types.FETCH_PROVIDER_REQUESTS_SUCCESS:
            {
                return {
                    ...state,
                    data: action.requests
                };
            }
        case types.FETCH_PROVIDER_REQUESTS_FAILED:
            {
                return {
                    ...state,
                    data: action.requests
                };
            }
        default:
            {
                return state;
            }
    }
}