import React from 'react';
import NavBar from '../../layout/navbar';

class Provider extends React.Component{

    constructor(props) {
        super(props);

        this.state = {

        };
    }

    render() {
        return (
            <div className="container-fluid">
                <div className="row">
                    <div className="col-12">
                        <NavBar />
                    </div>
                    <div className="col-12">
                        Provider profile
                    </div>
                </div>
            </div>
        );
    }

}

export default Provider;