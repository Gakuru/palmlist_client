import React from 'react';

import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';

import { user } from '../../utils/util';
import NavBar from '../../layout/navbar';

import * as providerActions from './actions';

class ProviderRequests extends React.Component{

    constructor(props) {
        super(props);

        this.state = {
            providerRequests: {}
        };

        this.getProviderRequests = this.getProviderRequests.bind(this);
        this.renderProviderRequests = this.renderProviderRequests.bind(this);

    }

    componentDidMount() {
        this.getProviderRequests();
    }

    getProviderRequests() {
        this.setState({providerRequests: this.props.providerActions.getProviderRequests(user.id)});
    }

    renderProviderRequests() {
        if (this.props.providerRequests) {
            const { providerRequests } = this.props;

            return providerRequests.map(request => {
                return (
                    <div key={request.request_id} className="row">
                        
                        <div className="col-12">
                            <div className="row">
                                <div className="col-1">
                                    Service:
                                </div>
                                <div className="col">
                                    {request.request_service_name}    
                                </div>
                            </div>    
                        </div>
                        
                        <div className="col-12">
                            <hr/>    
                        </div>
                        
                        <div className="col-12">
                            <div className="row">
                                <div className="col-1">
                                    Client:
                                </div>
                                <div className="col">
                                    {request.client_last_name}
                                </div>
                            </div>
                        </div>

                        <div className="col-12">
                            <hr/>    
                        </div>
                        
                        <div className="col-12">
                            <div className="row">
                                <div className="col-1">
                                    Location:
                                </div>
                                <div className="col">
                                    relative physical location of client
                                </div>
                            </div>
                        </div>
                        
                        <div className="col-12">
                            <hr/>    
                        </div>
                        
                        <div className="col-12">
                            <div className="row">
                                <div className="col-1">
                                    Distance:
                                </div>
                                <div className="col">
                                    approximate distance to client
                                </div>
                            </div>
                        </div>

                        <div className="col-12">
                            <hr/>    
                        </div>
                        
                        <div className="col-12">
                            <div className="row">
                                <div className="col-1">
                                    <button className="btn btn-danger">Decline</button>
                                </div>
                                <div className="col-1">
                                    <button className="btn btn-primary">Accept</button>
                                </div>
                            </div>
                        </div>

                    </div>
                );
            });

        } else {
            return null;
        }
    }

    render() {        
        return (
            <div className="container-fluid">
                <div>
                    <NavBar />    
                </div>
                <div>
                    <hr/>
                </div>
                <div className="breadcrumb">
                    Pending Requests
                </div>
                <div style={{marginTop:5}}>
                    {this.renderProviderRequests()}
                </div>
            </div>
        );
    }
}

const mapStateToProps = (store, ownProps) => {
    return {
        providerRequests: store.providerRequests.data,
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        providerActions: bindActionCreators(providerActions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProviderRequests);
