import React from 'react';

import {Text, Email, Tel, Password, Checkbox} from '../../widgets/Inputs';

const renderError = (user) => {
    if (user.data) {
        if (user.data.err) {
            return (
                <div className="alert alert-danger">{user.data.err.message}</div>
            )
        } else {
            return null;
        }
    }
}

export const SignUpForm = ({handleSignup, handleInputChange, user}) => {
    return (
        <div className="signup">
            <form onSubmit={handleSignup}>
                <fieldset>
                    <legend>Signup</legend>

                    {renderError(user)}

                    <div className="row">
                        <div className="col-sm-6 com-md-6 col-lg-6">
                            <Text
                                name="firstName"
                                label="First Name"
                                required={true}
                                onChange={handleInputChange}/>
                        </div>

                        <div className="col-sm-6 com-md-6 col-lg-6">
                            <div className="form-group">
                                <Text
                                    name="lastName"
                                    label="Last Name"
                                    required={true}
                                    onChange={handleInputChange}/>
                            </div>
                        </div>
                    </div>

                    <div className="form-group">
                        <Email name="email" label="Email" required={true} onChange={handleInputChange}/>
                    </div>

                    <div className="form-group">
                        <Tel name="phone" label="Phone" required={true} onChange={handleInputChange}/>
                    </div>

                    <div className="form-group">
                        <Password
                            name="password"
                            label="Password"
                            required={true}
                            onChange={handleInputChange}/>
                    </div>

                    <div className="form-group">
                        <Checkbox
                            name="isProvider"
                            label="Become a Provider"
                            value={false}
                            checked={false}
                            onChange={handleInputChange}/>
                    </div>

                    <div className="form-group">
                        <div className="float-right">
                            <button className="btn btn-primary" type="submit">
                                Sign me up
                            </button>
                        </div>
                    </div>

                </fieldset>
            </form>
        </div>
    );
}