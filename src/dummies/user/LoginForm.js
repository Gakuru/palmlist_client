import React from 'react';

import {Email, Password} from '../../widgets/Inputs';

const renderError = (user) => {
    if (user) {
        if (!user.success) {
            return (
                <div className="alert alert-danger">{user.message}</div>
            )
        } else {
            return null;
        }
    }
}

export const LoginForm = ({handleLogin, handleInputChange, user}) => {
    return (
        <div className="signup">
            <form onSubmit={handleLogin}>
                <fieldset>
                    <legend>Login</legend>

                    {renderError(user)}

                    <div className="form-group">
                        <Email name="email" label="Email" required={true} onChange={handleInputChange}/>
                    </div>

                    <div className="form-group">
                        <Password
                            name="password"
                            label="Password"
                            required={true}
                            onChange={handleInputChange}/>
                    </div>

                    <div className="form-group">
                        <div className="float-right">
                            <button className="btn btn-primary" type="submit">
                                Log me in
                            </button>
                        </div>
                    </div>

                </fieldset>
            </form>
        </div>
    );
}