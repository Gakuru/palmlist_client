import React from 'react';
import { Link } from 'react-router-dom';

import { user,logoutUser } from '../utils/util';

class NavBar extends React.Component{

    constructor(props) {
        super(props);

        this.renderLinks = this.renderLinks.bind(this);
        this.renderNavBar = this.renderNavBar.bind(this);
    }

    renderNavBar() {
        if (user) {
            return (
                <nav className="navbar navbar-expand-lg bg-light rounded">
                    <a className="navbar-brand" href="/">Palmlist</a>
                        {this.renderLinks()}
                    <ul className="navbar-nav ml-auto">
                        <li className="nav-item">
                            <a className="nav-link">{user.lastName}</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" style={{cursor:'pointer'}} onClick={()=>logoutUser()} >Logout</a>
                        </li>
                    </ul>
                </nav>
            );
        } else {
            window.location = '/login';
        }
    }

    renderLinks() {
        if (user.type === 'provider') {
            return (
                <ul className="navbar-nav mr-auto">
                    <li className="nav-item active">
                        <Link to='/' className="nav-link">Find Providers</Link>
                    </li>
                    <li className="nav-item">
                        <Link to={`/${user.lastName.toLowerCase()}/services`} className="nav-link">Your Services</Link>
                    </li>
                    <li className="nav-item">
                        <Link to={`/${user.lastName.toLowerCase()}/requests`} className="nav-link">Your Requests</Link>
                    </li>
                </ul>
            )
        } else {
            return (
                <ul className="navbar-nav mr-auto">
                        <li className="nav-item">
                            <Link to='/' className="nav-link">Find Providers</Link>
                        </li>    
                        <li className="nav-item">
                            <Link to={`/${user.lastName.toLowerCase()}/requests`} className="nav-link">Your Requests</Link>
                        </li>
                    </ul>
            );
        }
    }

    render() {
        return (
            <div>
                <div>
                    {this.renderNavBar()}
                </div>
                <div>
                    {this.props.children}
                </div>
            </div>
        );
    }

}

export default NavBar;