import React from 'react';

import {Route, Switch} from 'react-router-dom';

import Login from '../components/user/login/login';
import Signup from '../components/user/signup/signup';
import FindServiceNearBy from '../components/sevices/service';

import Provider from '../components/provider/provider';
import ProviderServices from '../components/provider/services';
import ProviderRequests from '../components/provider/requests';
import AddProviderService from '../components/provider/addService';

export const Routes = () => {
    return (
        <Switch>
            <Route exact path="/" component={FindServiceNearBy} />
            
            <Route exact path="/login" component={Login}/>
            <Route exact path="/signup" component={Signup} />
            
            <Route exact path="/:user" component={Provider} />
            <Route exact path="/:user/services" component={ProviderServices} />
            <Route exact path="/:user/requests" component={ProviderRequests} />
            <Route exact path="/:user/services/add" component={AddProviderService} />

        </Switch>
    );
}